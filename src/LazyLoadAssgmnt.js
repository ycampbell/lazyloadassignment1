import React, {Suspense} from "react";
import "./LazyLoadstyles.css"
const DisplayItem= React.lazy(()=> import("./DisplayItem"));


class Lazy extends React.Component{
    constructor(props){
        super(props);
        this.state= {
            list: [],
            count: 0,
        };
        this.handleClick=this.handleClick.bind(this) 
    }

    componentDidMount(){
        fetch("https://prof3ssorst3v3.github.io/media-sample-files/products.json")
        .then(response => response.json())
        .then(response => {
            this.setState({
                list : response
            })
        })
    }

    handleClick(){
        console.log("clicked")
        this.setState( (initial) => { 
         return {count: initial.count + 2 }
         });
    }

    render() {

        return(
            <div className="flex-container">
            <div className="grid-container">
                {this.state.list.slice(0, this.state.count).map((item) => {
                    return(
                    <Suspense key={item.id} class="cursor" fallback={<div className="load" style={{cursor:"wait"}}></div>}>
                         <div className="c">
                        <DisplayItem key={item.id} thing={item}/>
                         </div>
                    </Suspense>
                   
                     )
                     })
                }
                 <div className="button-container">
                <button type="button" className="btn-load" onClick={this.handleClick}>Load More</button>
                </div>
                </div>
               
                
            </div>
        )
    }
}


export default Lazy